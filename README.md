# fgo-crawler

# Install Requirements
```bash
sudo python3 -m pip install -r requirements.txt
```

# Run program
```bash
python3 main.py [lower_bound [upper_bound]]
```
Only character with number ID (e.g. No.1, not No.1b) within range `[lower_bound, upper_bound]`
will be fetched. By default `lower_bound=-1` and `upper_bound=INF`.

Please backup `data.csv` before run it.