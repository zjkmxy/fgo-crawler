#!/usr/bin/env python3

import grequests
from bs4 import BeautifulSoup
import re
import json
from sys import argv


CSV_DELI = ','


class Servant:
    def __init__(self, **kwargs):
        for key, value in kwargs.items():
            self.__setattr__(key, value)
        self.arts_buff = '0.0%'
        self.buster_buff = '0.0%'
        self.quick_buff = '0.0%'
        self.atk_buff = '0.0%'
        self.special_buff = '0.0%'
        self.critical_buff = '0.0%'
        self.damage_buff = '0.0%'
        self.star_generation_extra = '0.0%'
        self.pic = ''
        self.np_classification = ''

    def __str__(self):
        ret = ''
        for key, value in self.__dict__.items():
            ret += ('%s="%s;"' % (key, value))
        return ret

    def to_csv(self, fields):
        ret = ''
        for key in fields:
            ret += str(self.__dict__[key]).replace(CSV_DELI, ';') + CSV_DELI
        return ret[:-1] + '\n'


def get_servant(req, charid, classes, star, np_color):
    soup = BeautifulSoup(req.text, features="html.parser")
    tables = soup.find_all('table')

    # Basic infos
    basic_info_table = tables[0]
    rows = basic_info_table.find_all('tr')
    attribute = rows[1].find_all('td')[3].text
    attribute = '天地人星兽'.find(attribute)
    alignments, traits, nickname = [tr.text for tr in rows[5].find_all('td')]
    cards = [img.attrs['src'].split('/')[-1] for img in rows[2].find_all('img')]
    quick_num, arts_num, buster_num = (cards.count('pattern_%s.png' % cnum) for cnum in ['01', '02', '03'])

    # HP & Atk
    hp_atk_table = tables[1]
    row_data = hp_atk_table.find_all('tr')[1]
    default_hp = row_data.find_all('td')[1].text
    default_atk = row_data.find_all('td')[6].text
    # delete unacquirable servants
    if int(default_hp) == 0:
        raise ValueError

    # NP & Hits
    np_hit_table = tables[4]
    row_data = np_hit_table.find_all('tr')[1]
    [arts_na, buster_na, quick_na, ex_na, np_na, nd,
     arts_hit, buster_hit, quick_hit, ex_hit] = [tr.text for tr in row_data.find_all('td')]
    # No.108 Iskandar
    arts_na = re.findall("([0-9.]+%)", arts_na)[-1]
    buster_na = re.findall("([0-9.]+%)", buster_na)[-1]
    quick_na = re.findall("([0-9.]+%)", quick_na)[-1]
    ex_na = re.findall("([0-9.]+%)", ex_na)[-1]
    np_na = re.findall("([0-9.]+%)", np_na)[-1]

    # filter 'hits'
    arts_hit = arts_hit[:-4]
    buster_hit = buster_hit[:-4]
    quick_hit = quick_hit[:-4]
    ex_hit = ex_hit[:-4]

    # Property & Hidden
    hidden_table = tables[5]
    row_data = hidden_table.find_all('tr')[1]
    star_generation = row_data.find_all('td')[7].text
    # No.81 Hyde
    star_generation = re.findall("([0-9.]+%)", star_generation)[0]

    # Label & Name
    def label_class(tag):
        return tag.has_attr('class') and tag.attrs['class'] == ['uk-text-bold']

    def name_class(tag):
        return tag.has_attr('class') and tag.attrs['class'] == ['uk-text-bold', 'uk-margin-left']

    id_str = soup.find_all(label_class)[0].text
    name = soup.find_all(name_class)[0].text
    id_val = re.match(r"^No.(?P<idval>[0-9]+)$", id_str)['idval']
    if id_val != charid:
        raise ValueError

    # Noble Phantasm
    np_upgraded = 1 if str(tables[6].tr)[:200] == str(tables[7].tr)[:200] else 0
    np_hit = tables[6].find_all("tr")[2].find_all("td")[2].text[:-4]
    np_lv1 = '0.0%'
    np_lv2 = '0.0%'
    np_lv3 = '0.0%'
    np_lv4 = '0.0%'
    np_lv5 = '0.0%'
    np_lv1_before = '0.0%'
    np_lv2_before = '0.0%'
    np_lv3_before = '0.0%'
    np_lv4_before = '0.0%'
    np_lv5_before = '0.0%'
    if int(np_hit) <= 0:
        # If 0 hits, not attack np
        pass
    else:
        # Before
        trs = tables[6].find_all("tr")
        for i in range(3, len(trs)):
            # For all possible rows
            tds = trs[i].find_all("td")
            if len(tds) <= 2:
                continue
            if re.search("进行.*攻击", tds[1].text):
                np_lv1_before = tds[2].text
                # No.15 Euryale
                np_lv2_before = tds[3].text if len(tds) >= 4 else np_lv1_before
                np_lv3_before = tds[4].text if len(tds) >= 5 else np_lv2_before
                np_lv4_before = tds[5].text if len(tds) >= 6 else np_lv3_before
                np_lv5_before = tds[6].text if len(tds) >= 7 else np_lv4_before
                break
        # After
        if np_upgraded == 1:
            trs = tables[7].find_all("tr")
            for i in range(3, len(trs)):
                tds = trs[i].find_all("td")
                if len(tds) <= 2:
                    continue
                if re.search("进行.*攻击", tds[1].text):
                    np_lv1 = tds[2].text
                    np_lv2 = tds[3].text if len(tds) >= 4 else np_lv1
                    np_lv3 = tds[4].text if len(tds) >= 5 else np_lv2
                    np_lv4 = tds[5].text if len(tds) >= 6 else np_lv3
                    np_lv5 = tds[6].text if len(tds) >= 7 else np_lv4
                    break
        else:
            np_lv1 = np_lv1_before
            np_lv2 = np_lv2_before
            np_lv3 = np_lv3_before
            np_lv4 = np_lv4_before
            np_lv5 = np_lv5_before

    np_color = np_color[0].lower()

    return Servant(id=id_val, name=name, nickname=nickname, class_type=classes, star=star,
                   arts_hit=arts_hit, buster_hit=buster_hit, quick_hit=quick_hit, ex_hit=ex_hit,
                   quick_na=quick_na, arts_na=arts_na, buster_na=buster_na, ex_na=ex_na,
                   np_na=np_na, nd=nd, buster_num=buster_num, arts_num=arts_num, quick_num=quick_num,
                   star_generation=star_generation, np_color=np_color,
                   default_hp=default_hp, default_atk=default_atk,
                   attribute=attribute, traits=traits, alignments=alignments,
                   np_upgraded=np_upgraded, np_hit=np_hit, np_lv1=np_lv1, np_lv2=np_lv2,
                   np_lv3=np_lv3, np_lv4=np_lv4, np_lv5=np_lv5, np_lv1_before=np_lv1_before,
                   np_lv2_before=np_lv2_before, np_lv3_before=np_lv3_before,
                   np_lv4_before=np_lv4_before, np_lv5_before=np_lv5_before)


def get_servant_list(lower_bound, upper_bound):
    ret = {}
    i = 1
    flag = True
    while flag:
        res = (grequests.get('https://fgo.umowang.com/servant/ajax?pn=%d' % j)
               for j in range(i, i + 20))
        res = grequests.map(res)
        for it in res:
            data = json.loads(it.text)['data']
            if data:
                for x in data:
                    try:
                        charid = int(x['charid'])
                    except ValueError:
                        continue
                    if charid < lower_bound or charid > upper_bound:
                        continue
                    ret[x['id']] = (x['charid'], x['classes'], x['star'], x['tprop'])
            else:
                flag = False
        i += 20
    return ret


def main():
    fields = ['id', 'name', 'nickname', 'class_type', 'star',
              'arts_hit', 'buster_hit', 'quick_hit', 'ex_hit',
              'quick_na', 'arts_na', 'buster_na', 'ex_na', 'np_na', 'nd',
              'arts_buff', 'buster_buff', 'quick_buff', 'atk_buff', 'special_buff',
              'critical_buff', 'damage_buff', 'buster_num',
              'arts_num', 'quick_num', 'star_generation', 
              'np_lv1', 'np_lv2', 'np_lv3', 'np_lv4', 'np_lv5', 'np_color',
              'default_hp', 'default_atk', 'star_generation_extra',
              'np_lv1_before', 'np_lv2_before', 'np_lv3_before', 'np_lv4_before', 'np_lv5_before',
              'np_upgraded', 'attribute', 'np_hit', 'pic', 'traits', 'alignments', 'np_classification']

    lower_bound = int(argv[1]) if len(argv) >= 2 else -1
    upper_bound = int(argv[2]) if len(argv) >= 3 else 100000000
    print("Fetching list......")
    servant_list = get_servant_list(lower_bound, upper_bound)

    pgb_total = len(servant_list)
    pgb_now = 0
    pgb_scale = 50

    def progressbar_draw():
        c = pgb_now / pgb_total
        j = int(c * pgb_scale)
        a = chr(9608) * j
        b = ' ' * (pgb_scale - j)
        c = c * 100
        print("\r{:^3.0f}%[{}{}]".format(c, a, b), end='')

    def progressbar_inc(_r, **_kwargs):
        nonlocal pgb_now
        pgb_now += 1
        progressbar_draw()

    url_ids = list(servant_list.keys())
    get_set = (grequests.get("https://fgo.umowang.com/servant/%s" % uid, callback=progressbar_inc)
               for uid in url_ids)
    print("Fetching servants......")
    res_set = grequests.map(get_set)
    print("\nFinished.")

    servants = []
    for i, (charid, classes, star, np_color) in enumerate(servant_list.values()):
        try:
            print('Analyzing', i, '...', end='')
            servant = get_servant(res_set[i], charid, classes, star, np_color)
            servants.append(servant)
            print(servant.id, servant.name)
        except KeyboardInterrupt:
            raise
        except:
            print('Invalid')

    servants.reverse()
    with open("data.csv", "w", encoding="utf-8") as f:
        f.write(CSV_DELI.join(fields) + '\n')
        for servant in servants:
            f.write(servant.to_csv(fields))


if __name__ == "__main__":
    main()
